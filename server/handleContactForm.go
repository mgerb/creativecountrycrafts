package main

import (
	"crypto/tls"

	"github.com/gin-gonic/gin"
	gomail "gopkg.in/gomail.v2"
)

func handleContactForm(c *gin.Context) {

	name, _ := c.GetPostForm("name")
	email, _ := c.GetPostForm("email")
	phone, _ := c.GetPostForm("phone")
	message, _ := c.GetPostForm("message")

	m := gomail.NewMessage()
	m.SetHeader("From", Conf.MailerEmail)
	m.SetHeader("To", Conf.MailerEmail)
	m.SetHeader("Subject", "New Contact - Creative Country Crafts")

	body := "<b>Name</b>: " + name + "<br>"
	body += "<b>Email</b>: " + email + "<br>"
	body += "<b>Phone</b>: " + phone + "<br>"
	body += "<b>Message</b>: " + message + "<br>"

	m.SetBody("text/html", body)

	d := gomail.NewDialer("smtp.gmail.com", 587, Conf.MailerEmail, Conf.MailerPassword)
	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}

	// Send the email to Bob, Cora and Dan.
	if err := d.DialAndSend(m); err != nil {
		panic(err)
	}

	c.File("./index.html")
}
