package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
)

type config struct {
	Password       string `json:"Password"`
	Port           int    `json:"port"`
	MailerEmail    string `json:"mailerEmail"`
	MailerPassword string `json:"mailerPassword"`
}

func getConfig() *config {

	log.Println("Reading config file...")

	file, err := ioutil.ReadFile("./config.json")

	if err != nil {
		log.Fatal(err)
	}

	log.Printf("%s\n", string(file))

	c := &config{}

	err = json.Unmarshal(file, c)

	if err != nil {
		log.Println(err)
	}

	return c
}
