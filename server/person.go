package main

import (
	"errors"

	"github.com/asdine/storm"
)

// Person -
type Person struct {
	ID    int `storm:"id,increment"`
	Email string
	Name  string
}

// Store - store a new person in database
func (p *Person) store(db *storm.DB) error {
	if p.Email == "" || p.Name == "" {
		return errors.New("Missing fields")
	}

	// save person to database
	return db.Save(p)
}

func getPeople(db *storm.DB) ([]Person, error) {

	var people []Person
	err := db.All(&people)

	if err != nil {
		return []Person{}, err
	}

	return people, nil
}
