package main

import "github.com/gin-gonic/gin"

func handleEmailForm(c *gin.Context) {
	name, _ := c.GetPostForm("name")
	email, _ := c.GetPostForm("email")

	person := &Person{
		Name:  name,
		Email: email,
	}

	person.store(DB)

	c.File("./success.html")
}
