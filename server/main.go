package main

import (
	"strconv"

	"github.com/asdine/storm"
	"github.com/gin-gonic/gin"
)

var (
	Conf *config
	DB   *storm.DB
)

func init() {
	Conf = getConfig()
}

func main() {
	router := gin.Default()
	registerRoutes(router)
	openDatabase()

	port := ":" + strconv.Itoa(Conf.Port)
	router.Run(port)
}

func openDatabase() {
	var err error
	DB, err = storm.Open("store.db")

	if err != nil {
		panic("Error opening database.")
	}
}

func registerRoutes(router *gin.Engine) {

	// serve static files
	router.Static("/css", "./css")
	router.Static("/fonts", "./fonts")
	router.Static("/images", "./images")
	router.Static("/js", "./js")

	// serve individual files
	router.GET("/", func(c *gin.Context) {
		c.File("./index.html")
	})
	router.GET("/form", func(c *gin.Context) {
		c.File("./form.html")
	})

	// handle email form
	router.POST("/form", handleEmailForm)

	// hanlde contact form
	router.POST("/contact", handleContactForm)

	// write data to csv file
	router.GET("/database/csv/:password", handleCsv)
}
