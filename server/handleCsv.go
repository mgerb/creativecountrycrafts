package main

import (
	"bytes"
	"encoding/csv"
	"strconv"

	"github.com/gin-gonic/gin"
)

func handleCsv(c *gin.Context) {

	if c.Param("password") != Conf.Password {
		c.AbortWithStatus(401)
		return
	}

	people, err := getPeople(DB)

	if err != nil {
		c.AbortWithStatus(401)
		return
	}

	b := &bytes.Buffer{}
	wr := csv.NewWriter(b)

	for _, p := range people {
		wr.Write([]string{strconv.Itoa(p.ID), p.Name, p.Email})
	}

	wr.Flush()

	c.Header("Content-Type", "text/csv")
	c.Writer.Write(b.Bytes())
}
